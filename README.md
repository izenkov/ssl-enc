# Friendly Bash script to encrypt/decrypt strings or files using Open SSL

Script successfully tested in the following environments:

 - **Ubuntu Linux 18.04 LTS**
 - **FreeBSD 12.0**
 - **Windows 10** (in Git for Windows Bash environment)

### Dependencies:

 - **Open SSL**

### Usage:

```
  ./ssl-enc.sh COMMAND
```
### ssl-enc.sh commands:

 - **file-enc** Encrypt file
 - **file-dec** Decrypt file
 - **str-enc** Encrypt string
 - **str-dec** Decrypt string
 - **help** Show help

### file-enc command

```
  Usage: ssl-enc.sh file-enc <src-fname> <dst-fname>
         ssl-enc.sh file-enc <src-fname> <dst-fname> <pas-fname>
         ssl-enc.sh file-enc <src-fname> <dst-fname> out:<pas-fname>
  Where: src-fname - Source (original) file name
         dst-fname - Destination (encrypted) file name
         pas-fname - File name containing the password
   Like: ssl-enc.sh enc-file ./secret.txt ./secret.txt.enc
         ssl-enc.sh file-enc ./secret.txt ./secret.txt.enc ./pass
         ssl-enc.sh file-enc ./secret.txt ./secret.txt.enc out:./pass
   Note: 'out:' prefix directs password generator output to 'pas-fname'
```
### file-dec command
```
  Usage: ssl-enc.sh file-dec <src-fname> <dst-fname> 
         ssl-enc.sh file-dec <src-fname> <dst-fname> <pas-fname> 
  Where: src-fname - Source (encrypted) file name 
         dst-fname - Destination (decrypted) file name 
         pas-fname - File name containing the password 
   Like: ssl-enc.sh file-dec ./secret.txt.enc ./secret.txt 
         ssl-enc.sh file-dec ./secret.txt.enc ./secret.txt ./pass 
```
### str-enc command
```
  Usage: ssl-enc.sh str-enc <src-string>
         ssl-enc.sh str-enc <src-string> <pas-fname>
         ssl-enc.sh str-enc <src-string> out:<pas-fname>
  Where: src-string - Source (original) string
         pas-fname  - File name containing the password
   Like: ssl-enc.sh str-enc 'Hello World!'
         ssl-enc.sh str-enc 'Hello World!' ./pass
         ssl-enc.sh str-enc 'Hello World!' out:./pass
   Note: 'out:' prefix directs password generator output to 'pas-fname'
```
### str-dec command
```
  Usage: ssl-enc.sh str-dec <src-string> 
         ssl-enc.sh str-dec <src-string> <pas-fname> 
  Where: src-string - Source (encrypted) string 
         pas-fname  - File name containing the password 
   Like: ssl-enc.sh str-dec 'U2FsdGVkX18KpePhHHheTvG9gNe8Mx7fZPclH95S77c=' 
         ssl-enc.sh str-dec 'U2FsdGVkX18KpePhHHheTvG9gNe8Mx7fZPclH95S77c=' ./pass 
```
### help command
```
  Usage: ssl-enc.sh help
         ssl-enc.sh help COMMAND
   Like: ssl-enc.sh help enc-str
```

### Encryption password <pas-fname> parameter

 - **omitted** Prompts user to enter encryption password
 - **pas-fname** Reads encryption password from 'pas-fname' file
 - **out:pas-fname** Generates encryption password and saves it to 'pas-fname' file

### Decryption password <pas-fname> parameter

 - **omitted** Prompts user to enter decryption password
 - **pas-fname** Reads decryption password from 'pas-fname' file

### Project files:

 - **ssl-enc.sh** Bash script
 - **tests.sh** Unit tests (strings and files)
 - **README.md** This file

### Defaults (openssl enc options)

 - **-base64** Output encrypted data in Base64 encoding
 - **-aes256** Use aes-256-cbc cipher
 - **-salt** Use salt (random unique token stored with each encrypted data)
 - **-md sha256** Use SHA256 to generate encryption key from password

## License

[MIT](LICENSE.md)

