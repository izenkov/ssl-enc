#!/usr/bin/env bash

# Open SSL based en/de cryption tool
# Author Igor P. Zenkov igor@zenkov.com

# constants

declare -r SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
declare -r SCRIPT_NAME=$(basename "$0")
declare -r SSL_ENC_VER='1.1.1 Feb 20, 2019'
declare -r SSL_ENC_COMMANDS='file-enc file-dec str-enc str-dec help'

# defaults

# length (bytes) of randomly generated password
# if you specify out:<pas-fname> argument
# default value is 16 (anything below 8 is not secure)
# binds to environment variable SSL_ENC_RANDOM_PASS_SIZE
declare -i CONFIG_RANDOM_PASS_SIZE=16 # 16 bytes

# answer yes on file overwrite (0-true/1-false)
# default value is 0
# binds to environment variable SSL_ENC_YES_ON_OVERWRITE
declare -i CONFIG_YES_ON_OVERWRITE=0  # false

# read config env variables

[[ ${SSL_ENC_RANDOM_PASS_SIZE} ]] && CONFIG_RANDOM_PASS_SIZE=${SSL_ENC_RANDOM_PASS_SIZE}
[[ ${SSL_ENC_YES_ON_OVERWRITE} ]] && CONFIG_YES_ON_OVERWRITE=${SSL_ENC_YES_ON_OVERWRITE}

# fix locale to suppress 'tr: Illegal byte sequence' messages in randpw() on BSD

export LC_CTYPE=C

# functions

function out() {
  printf '%s\n' "$1"
}

function error() {
  echo "Error: $1"
  exit 1
}

function assert_file() {
  local fname=$1
  [[ ! -f $fname ]] && error "file $fname not found"
}

function assert_action() {
  local action=$1
  [[ $action != '-e' && $action != '-d' ]] && error "unknown parameter $action"
}

function assert_args() {
  local want=$1
  local actual=$2
  local fn_help=$3

  if [ "$actual" -lt "$want" ]; then
    echo "Error: missing required arguments"
    "$fn_help"
    exit 1
  fi
}

function confirm() {
  local prompt=$1
  local yes=$2
  [[ $yes -eq 1 ]] && return 0
  printf 'Are you sure, you want to %s? [Y/n]:' "$prompt"
  read -r -n 1 yn; echo ""
  [[ $yn == 'Y' ]]
}

function confirm_overwrite() {
  local fname=$1
  if [[ -f $fname ]]; then
    if ! confirm "overwrite $fname" "$CONFIG_YES_ON_OVERWRITE"; then
      exit 0
    fi
  fi
}

# update and validate pass variable
function set_pass() {
  local pass2
  echo -n 'Enter password: '
  read -r -s pass
  echo
  echo -n 'Verify password: '
  read -r -s pass2
  echo
  [[ $pass != "$pass2" ]] && error 'password verification failed'
}

# update pass variable
function get_pass() {
  echo -n 'Enter password: '
  read -r -s pass
  echo
}

function randpw() {
  < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-$CONFIG_RANDOM_PASS_SIZE}; echo;
}

function str_end() {
  local str=$1
  local pos=$2

  local len=$((${#str}-pos))
  printf '%s' "${str:$pos:$len}"
}

function gen_pass_file() {
  local -r pas_fname=$1
  local -r pas_string=$(randpw)
  printf '%s' "$pas_string" > "$pas_fname"
}

function ssl_crypt_file() {
  local src_fname=$1
  local dst_fname=$2
  local enc_dec=$3
  local pass_fname=$4

  assert_action "$enc_dec"
  assert_file "$src_fname"
  confirm_overwrite "$dst_fname"

  local pass
  if [[ $pass_fname ]]; then
    assert_file "$pass_fname"
    pass="$pass_fname"
  else
    [[ $enc_dec == '-e' ]] && set_pass pass
    [[ $enc_dec == '-d' ]] && get_pass pass
  fi

  openssl enc -md sha256 -aes256 -base64 "$enc_dec" \
    -in "$src_fname" \
    -pass pass:"$pass" \
    -out "$dst_fname"
}

function ssl_crypt_str() {
  local src_str=$1
  local enc_dec=$2
  local pass_fname=$3

  assert_action "$enc_dec"

  local pass
  if [[ $pass_fname ]]; then
    assert_file "$pass_fname"
    pass="$pass_fname"
  else
    [[ $enc_dec == '-e' ]] && set_pass pass
    [[ $enc_dec == '-d' ]] && get_pass pass
  fi

  # Note: Trailing newline is required for the base64 string being decrypted
  echo "$src_str" | openssl enc \
    -md sha256 -aes256 -base64 "$enc_dec" \
    -pass pass:"$pass"
}

function help_cmd() {
  usage
  for cmd in "$@"; do
    out "  $cmd"
    cmd=${cmd/-/_} # enc-str -> enc_str
    "usage_$cmd"
  done
}

# commands

function cmd_file_enc() {
  local src_fname=$1
  local dst_fname=$2
  local pass_fname=$3

  assert_args 2 $# 'usage_enc_file'
  assert_file "$src_fname"

  if [[ ${pass_fname:0:4} == 'out:' ]]; then
    pass_fname=$(str_end "$pass_fname" 4)
    confirm_overwrite "$pass_fname"
    gen_pass_file "$pass_fname"
  fi

  ssl_crypt_file "$src_fname" "$dst_fname" -e "$pass_fname"
}

function cmd_file_dec() {
  local src_fname=$1
  local dst_fname=$2
  local pass_fname=$3

  assert_args 2 $# 'usage_dec_file'
  assert_file "$src_fname"

  ssl_crypt_file "$src_fname" "$dst_fname" -d "$pass_fname"
}

function cmd_str_enc() {
  local src_str=$1
  local pass_fname=$2

  assert_args 1 $# 'usage_enc_str'

  if [[ ${pass_fname:0:4} == 'out:' ]]; then
    pass_fname=$(str_end "$pass_fname" 4)
    confirm_overwrite "$pass_fname"
    gen_pass_file "$pass_fname"
  fi

  ssl_crypt_str "$src_str" -e "$pass_fname"
}

function cmd_str_dec() {
  local src_str=$1
  local pass_fname=$2

  assert_args 1 $# 'usage_dec_str'

  ssl_crypt_str "$src_str" -d "$pass_fname"
}

function cmd_help() {
  if [[ $# -eq 0 ]]; then
    help_cmd $SSL_ENC_COMMANDS
  else
    help_cmd "$@"
  fi
} 

# usage

function usage_file_enc() {
  out
  out "  Usage: $SCRIPT_NAME file-enc <src-fname> <dst-fname>"
  out "         $SCRIPT_NAME file-enc <src-fname> <dst-fname> <pas-fname>"
  out "         $SCRIPT_NAME file-enc <src-fname> <dst-fname> out:<pas-fname>"
  out "  Where: src-fname - Source (original) file name"
  out "         dst-fname - Destination (encrypted) file name"
  out "         pas-fname - File name containing the password"
  out "   Like: $SCRIPT_NAME file-enc ./secret.txt ./secret.txt.enc"
  out "         $SCRIPT_NAME file-enc ./secret.txt ./secret.txt.enc ./pass"
  out "         $SCRIPT_NAME file-enc ./secret.txt ./secret.txt.enc out:./pass"
  out "   Note: 'out:' prefix directs password generator output to 'pas-fname'"
  out
} 

function usage_file_dec() {
  out
  out "  Usage: $SCRIPT_NAME file-dec <src-fname> <dst-fname>"
  out "         $SCRIPT_NAME file-dec <src-fname> <dst-fname> <pas-fname>"
  out "  Where: src-fname - Source (encrypted) file name"
  out "         dst-fname - Destination (decrypted) file name"
  out "         pas-fname - File name containing the password"
  out "   Like: $SCRIPT_NAME file-dec ./secret.txt.enc ./secret.txt"
  out "         $SCRIPT_NAME file-dec ./secret.txt.enc ./secret.txt ./pass"
  out
}

function usage_str_enc() {
  out
  out "  Usage: $SCRIPT_NAME str-enc <src-string>"
  out "         $SCRIPT_NAME str-enc <src-string> <pas-fname>"
  out "         $SCRIPT_NAME str-enc <src-string> out:<pas-fname>"
  out '  Where: src-string - Source (original) string'
  out '         pas-fname  - File name containing the password'
  out "   Like: $SCRIPT_NAME str-enc 'Hello World!'"
  out "         $SCRIPT_NAME str-enc 'Hello World!' ./pass"
  out "         $SCRIPT_NAME str-enc 'Hello World!' out:./pass"
  out "   Note: 'out:' prefix directs password generator output to 'pas-fname'"
  out
} 

function usage_str_dec() {
  out
  out "  Usage: $SCRIPT_NAME str-dec <src-string>"
  out "         $SCRIPT_NAME str-dec <src-string> <pas-fname>"
  out '  Where: src-string - Source (encrypted) string'
  out '         pas-fname  - File name containing the password'
  out "   Like: $SCRIPT_NAME str-dec 'U2FsdGVkX18KpePhHHheTvG9gNe8Mx7fZPclH95S77c='"
  out "         $SCRIPT_NAME str-dec 'U2FsdGVkX18KpePhHHheTvG9gNe8Mx7fZPclH95S77c=' ./pass"
  out
} 

function usage_help() {
  out
  out "  Usage: $SCRIPT_NAME help"
  out "         $SCRIPT_NAME help COMMAND"
  out "   Like: $SCRIPT_NAME help str-enc"
  out
} 

function usage() {
  out "$SCRIPT_NAME $SSL_ENC_VER"
  out "Usage: $SCRIPT_NAME COMMAND"
  out
  out 'Commands:'
  out '  file-enc Encrypt file'
  out '  file-dec Decrypt file'
  out '  str-enc  Encrypt string'
  out '  str-dec  Decrypt string'
  out '  help     Show help'
  out
} 

function main() {
  local cmd=$1
  shift 1
  case $cmd in
    'file-enc')
      cmd_file_enc "$@"
      ;;
    'file-dec')
      cmd_file_dec "$@"
      ;;
    'str-enc')
      cmd_str_enc "$@"
      ;;
    'str-dec')
      cmd_str_dec "$@"
      ;;
    'help')
      cmd_help "$@"
      ;;
    *)
      usage
  esac
}

# main

main "$@"

