#!/usr/bin/env bash

# constants

declare -r SCRIPT_NAME=$(basename "$0")
declare -r DEBUG=0 # set to 1 for debugging
declare -r TMP_PAS_MASK="/tmp/$SCRIPT_NAME.pas.XXXXXXXX" # password file
declare -r TMP_SRC_MASK="/tmp/$SCRIPT_NAME.src.XXXXXXXX" # source file
declare -r TMP_ENC_MASK="/tmp/$SCRIPT_NAME.enc.XXXXXXXX" # encoded file
declare -r TMP_DEC_MASK="/tmp/$SCRIPT_NAME.dec.XXXXXXXX" # decoded file

# global vars

declare PAS_FNAME
declare SRC_FNAME
declare ENC_FNAME
declare DEC_FNAME

declare -a ARR_STRINGS=()

ARR_STRINGS[0]=''
ARR_STRINGS[1]=' '
ARR_STRINGS[2]='Hello World!'
ARR_STRINGS[3]='Привет Мир!'
ARR_STRINGS[4]='こんにちは世界'
ARR_STRINGS[5]='{"price": 99.99, "ccy": "USD", "http": 200, "iserr": false}'

# functions

function dbg() {
  local msg=$1
  if [ "$DEBUG" -eq 1 ]; then
    echo "$msg"
  fi
}

function str_end() {
  local str=$1
  local pos=$2

  local len=$((${#str}-$pos))
  printf "${str:$pos:$len}"
}

function get_enc_pass_fname() {
  local pas_fname=$1
  if [ "${pas_fname:0:4}" == 'out:' ]; then
    printf "$(str_end "$pas_fname" 4)"
  else
    printf "$pas_fname"
  fi
}

# setup

function test_setup() {
  echo 'Test setup ...'

  PAS_FNAME=$(mktemp $TMP_PAS_MASK)
  SRC_FNAME=$(mktemp $TMP_SRS_MASK)
  ENC_FNAME=$(mktemp $TMP_ENC_MASK)
  DEC_FNAME=$(mktemp $TMP_DEC_MASK)

  printf '12345678' >> "$PAS_FNAME"
}

# teardown

function test_teardown() {
  echo
  echo 'Test teardown ...'
  rm "$PAS_FNAME"
  rm "$SRC_FNAME"
  rm "$ENC_FNAME"
  rm "$DEC_FNAME"
}

function test_enc_dec_str() {
  set -e

  local src_string=$1
  local enc_pas_fname=$2 # encrypt password file name
  local dec_pas_fname=$3 # decrypt password file name

  dbg "Encrypting '$src_string' ..."
  local enc_string=$(./ssl-enc.sh str-enc "$src_string" "$enc_pas_fname")

  dbg "Password: '$(cat $(get_enc_pass_fname $enc_pas_fname))'"

  dbg "Decrypting '$enc_string' ..."
  local dec_string=$(./ssl-enc.sh str-dec "$enc_string" "$dec_pas_fname")

  if [ "$src_string" == "$dec_string" ]; then
    printf 'PASS'
  else
    printf 'FAIL'
  fi

  printf " string '$src_string' \n"

  set +e
}

function test_enc_dec_file() {
  set -e

  local src_string=$1
  local enc_pas_fname=$2 # encrypt password file name
  local dec_pas_fname=$3 # decrypt password file name

  dbg "Writing content to '$SRC_FNAME' ..."
  printf "$src_string" > "$SRC_FNAME"

  dbg "Encrypting '$SRC_FNAME' ..."
  ./ssl-enc.sh file-enc "$SRC_FNAME" "$ENC_FNAME" "$enc_pas_fname"

  dbg "Password: '$(cat $(get_enc_pass_fname $enc_pas_fname))'"

  dbg "Decrypting '$ENC_FNAME' ..."
  ./ssl-enc.sh file-dec "$ENC_FNAME" "$DEC_FNAME" "$dec_pas_fname"

  dbg "Reading content from '$DEC_FNAME' ..."
  dec_string=$(cat "$DEC_FNAME")

  if [ "$src_string" == "$dec_string" ]; then
    printf 'PASS'
  else
    printf 'FAIL'
  fi

  printf " string '$src_string' \n"

  set +e
}

function test_enc_dec_str_all() {
  local enc_pas_fname=$1 # encrypt password file name
  local dec_pas_fname=$2 # decrypt password file name
  local title=$3

  printf "\n Strings: $title \n\n"

  for str in "${ARR_STRINGS[@]}"; do
    test_enc_dec_str "$str" "$enc_pas_fname" "$dec_pas_fname"
  done
}

function test_enc_dec_file_all() {
  local enc_pas_fname=$1 # encrypt password file name
  local dec_pas_fname=$2 # decrypt password file name
  local title=$3

  printf "\n Files: $title \n\n"

  for str in "${ARR_STRINGS[@]}"; do
    test_enc_dec_file "$str" "$enc_pas_fname" "$dec_pas_fname"
  done
}

function test_run() {
  test_enc_dec_str_all "$PAS_FNAME" "$PAS_FNAME"
  test_enc_dec_file_all "$PAS_FNAME" "$PAS_FNAME"
  test_enc_dec_str_all  "out:$PAS_FNAME" "$PAS_FNAME" '(gen password)'
  test_enc_dec_file_all "out:$PAS_FNAME" "$PAS_FNAME" '(gen password)'
}

# main

export SSL_ENC_YES_ON_OVERWRITE=1 # do not ask questions (see ssl-enc.conf)

test_setup
test_run
test_teardown

